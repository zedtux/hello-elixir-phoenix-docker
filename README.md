# Hello

My first hello world application written in [Elixir](http://elixir-lang.github.io)
with [Phoenix](http://phoenixframework.org) web framework using
[Docker](https://www.docker.com).

Thanks to [@danguita](https://twitter.com/danguita)'s [blog article](https://davidanguita.name/articles/dockerizing-a-phoenix-project/)
I could manage really quickly to get my environment working.

## Usage

To start the Phoenix server:

  * Build the docker image: `docker-compose build app`
  * Start the stack: `docker-sync-stack start`
  * Create and migrate your database with `docker-compose exec app mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `docker-compose run --rm --workdir=/application/assets app npm install`
  * Restart the Phoenix endpoint with `docker-compose restart app`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

You can also go to http://localhost:4000/hello/yourname which is a first dynamic
page I did following the tutorial from https://hexdocs.pm/phoenix/overview.html.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Running the tests

You have two ways:

```bash
docker-compose exec app bash -c 'MIX_ENV=test mix test'
```

Or

```bash
docker-compose run --rm -e MIX_ENV=test app mix test
```

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
