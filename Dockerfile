# Latest version of Erlang-based Elixir installation: https://hub.docker.com/_/elixir/
# Linux distribution: Debian Jessie
FROM elixir:1.5.2-slim
MAINTAINER zedtux, zedtux@zedroot.org

# Create and set home directory
ENV APP_HOME /application
WORKDIR $APP_HOME

# Install Nodejs and NPM
# Version 9 is not able to install the fsevents package
RUN apt-get update && \
    apt-get install -y curl && \
    curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get install -y nodejs inotify-tools && \
    apt-get remove --purge -y curl && \
    rm -rf /var/lib/apt/lists/*

# Install hex (Elixir package manager)
RUN mix local.hex --force

# Install rebar (Erlang build tool)
RUN mix local.rebar --force

# Copy all dependencies files
COPY mix.* ./

# Install all dependencies
RUN mix deps.get

# Compile all dependencies
RUN mix deps.compile

# Copy all application files
COPY . .

# Compile the entire project
RUN mix compile

# Run Ecto migrations and Phoenix server as an initial command
CMD mix do ecto.migrate, phx.server
